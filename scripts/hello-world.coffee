# Description:
#   Greet the world
#
# Commands:
#   hubot hello world - Say hello to the world

module.exports = (robot) ->
  robot.respond /hello world/i, (msg) ->
    msg.send "Hello, World!"
